#include "stm32f4xx_conf.h"
#include "buffer.h"
#include "usart.h"
#include "pwm.h"
#include <string.h>
#include <stdio.h>

void addToBuffer(Buffer &buffer, uint8_t receivedByte)
{
    if (buffer.nLastWritedElem < BUFFER_SIZE)
    {
        buffer.elem[buffer.nLastWritedElem] = receivedByte;
        buffer.nLastWritedElem++;
    }
    else
    {
        USART_SendString(USART, "Buffer is full! \r\nBuffer was cleared! \r\nWrite command again. \r\n");
        buffer.nLastWritedElem = 0;
    }
}

void parseBuffer(Buffer &buffer, int captDutyCycle, int captFrequency)
{
    char output[BUFFER_SIZE];
    if (IS_SET == OK)
    {
        if (IS_FREQ == OK)
        {
            unsigned int frequency, period, prescaler, dutyCycle;
            sscanf(buffer.elem + 4 + 10, "%d", &frequency);

            if (frequency > 0)
            {
                // get old parameters to calc dutyCycle
                period = (TIM4->ARR) + 1;
                dutyCycle = (TIM4->CCR1) * 100 / period;
                // set new parameters
                calcTimParameters(period, prescaler, frequency);
                TIM4->ARR = period;
                TIM4->PSC = prescaler;
                setDutyCycle(TIM4, dutyCycle);

                sprintf(output, "Current frequency is %d Hz\r\n", frequency);
            }
            else
            {
                sprintf(output, "%s", "NOP. I do not think so.");
            }
        }
        else if (IS_DUTY == OK)
        {
            unsigned int dutyCycle;

            sscanf(buffer.elem + 4 + 11, "%d", &dutyCycle);
            if (dutyCycle > 0 && dutyCycle <= 100)
            {
                setDutyCycle(TIM4, dutyCycle);

                sprintf(output, "Current duty cycle is %d percent\r\n", dutyCycle);
            }
            else
            {
                sprintf(output, "%s", "NOP. I do not think so.");
            }
        }
    }
    else if (IS_GET == OK)
    {
        if (IS_FREQ == OK)
        {
            sprintf(output, "Current frequency is %d Hz\r\n", captFrequency);
        }
        else if (IS_DUTY == OK)
        {
            sprintf(output, "Current duty cycle is %d percent +- 2\r\n", captDutyCycle);
        }
    }
    else if (IS_HELP == OK)
    {
        sprintf(output, "%s", "Commands list: \r\nset\r\n       frequency [Hz]\r\n       duty cycle [%]\r\nget\r\n       frequency [Hz]\r\n       duty cycle [%]\r\n\r\n");
    }
    else
    {
        sprintf(output, "%s", "Write help to see available commands \r\n");
    }

    USART_SendString(USART, output);
}
