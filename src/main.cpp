#include "stm32f4xx_conf.h"
#include "buffer.h"
#include "usart.h"
#include "pwm.h"

Buffer buffer;
int dutyCycle, frequency;

void GPIO_Initialize();
void USART_Initialize();
void TIM_Initialize();
void NVIC_Initialize();

int main(void)
{
    //Initialize Stuff
    GPIO_Initialize();
    USART_Initialize();
    TIM_Initialize();
    NVIC_Initialize();
    //Loop
    while(1){
        if (buffer.isReady)
        {
            parseBuffer(buffer, dutyCycle, frequency);
            buffer.nLastWritedElem = 0;
            buffer.isReady = 0;
            USART_ITConfig(USART, USART_IT_RXNE, ENABLE);
            TIM_ITConfig(TIM3, TIM_IT_CC2, ENABLE);
        }
    };
}

void GPIO_Initialize()
{
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA |
                           RCC_AHB1Periph_GPIOB, ENABLE);

    GPIO_InitTypeDef gpioUart;
    gpioUart.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1;
    gpioUart.GPIO_Mode  = GPIO_Mode_AF;
    gpioUart.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOA, &gpioUart);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_UART4);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_UART4);

    GPIO_InitTypeDef gpioTim4;
    gpioTim4.GPIO_Pin    = GPIO_Pin_6;
    gpioTim4.GPIO_Mode   = GPIO_Mode_AF;
    gpioTim4.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_Init(GPIOB, &gpioTim4);

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_TIM4);

    GPIO_InitTypeDef gpioTim3;
    gpioTim3.GPIO_Pin    = GPIO_Pin_7;
    gpioTim3.GPIO_Mode   = GPIO_Mode_AF;
    gpioTim3.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_Init(GPIOA, &gpioTim3);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_TIM3);
}

void TIM_Initialize()
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4 |
                           RCC_APB1Periph_TIM3, ENABLE);

    unsigned int period, prescaler;
    calcTimParameters(period, prescaler, REQUIRED_FREQUENCY);

    TIM_TimeBaseInitTypeDef baseTim;
    baseTim.TIM_Prescaler   = prescaler;
    baseTim.TIM_Period      = period;
    baseTim.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM4, &baseTim);

    TIM_OCInitTypeDef pwdTim;
    pwdTim.TIM_OCMode       = TIM_OCMode_PWM1;
    pwdTim.TIM_OutputState  = TIM_OutputState_Enable;
    pwdTim.TIM_Pulse        = baseTim.TIM_Period / 2; //Just for lulz
    pwdTim.TIM_OCPolarity   = TIM_OCPolarity_High;
    TIM_OC1Init(TIM4, &pwdTim);
    TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);

    TIM_Cmd(TIM4, ENABLE);

    TIM_ICInitTypeDef inputCompareTim;
    inputCompareTim.TIM_Channel = TIM_Channel_2;
    inputCompareTim.TIM_ICPolarity = TIM_ICPolarity_Rising;
    inputCompareTim.TIM_ICFilter = 0x0;
    inputCompareTim.TIM_ICSelection = TIM_ICSelection_DirectTI;

    TIM_PWMIConfig(TIM3, &inputCompareTim);

    TIM_SelectInputTrigger(TIM3, TIM_TS_TI2FP2);
    TIM_SelectSlaveMode(TIM3, TIM_SlaveMode_Reset);
    TIM_SelectMasterSlaveMode(TIM3, TIM_MasterSlaveMode_Enable);

    TIM_Cmd(TIM3, ENABLE);

    TIM_ITConfig(TIM3, TIM_IT_CC2, ENABLE);
}

void USART_Initialize()
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);

    USART_InitTypeDef usart;
    USART_StructInit(&usart);
    USART_Init(USART, &usart);

    USART_Cmd(USART, ENABLE);

    USART_ITConfig(USART, USART_IT_RXNE, ENABLE);
}

void NVIC_Initialize()
{
    NVIC_InitTypeDef nvicTim3;
    nvicTim3.NVIC_IRQChannel = TIM3_IRQn;
    nvicTim3.NVIC_IRQChannelPreemptionPriority = 0;
    nvicTim3.NVIC_IRQChannelSubPriority = 1;
    nvicTim3.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvicTim3);

    NVIC_InitTypeDef nvicUsart;
    nvicUsart.NVIC_IRQChannel = UART4_IRQn;
    nvicUsart.NVIC_IRQChannelPreemptionPriority = 0;
    nvicUsart.NVIC_IRQChannelSubPriority = 2;
    nvicUsart.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvicUsart);
}

extern "C" void UART4_IRQHandler()
{
    if (USART_GetITStatus(USART, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART, USART_IT_RXNE);

        uint8_t receivedByte;
        receivedByte = USART_ReceiveData(USART);

        addToBuffer(buffer, receivedByte);
        if (receivedByte == LINE_END)
        {
            buffer.isReady = 1;
            addToBuffer(buffer, 0); //null-terminated string
            USART_ITConfig(USART, USART_IT_RXNE, DISABLE);
            TIM_ITConfig(TIM3, TIM_IT_CC2, DISABLE);
        }
    }
}

extern "C" void TIM3_IRQHandler()
{
    if (TIM_GetITStatus(TIM3, TIM_IT_CC2) != RESET)
    {
        TIM_ClearITPendingBit(TIM3, TIM_IT_CC2);

        int captredValue;
        captredValue = TIM_GetCapture2(TIM3);

        if (captredValue != 0)
        {
            dutyCycle = (TIM_GetCapture1(TIM3) * 100) / captredValue;
            frequency = CORE_CLOCK / captredValue;
        }
        else
        {
            dutyCycle = 0;
            frequency = 0;
        }
    }
}
