#include "stm32f4xx_conf.h"
#include "pwm.h"

void calcTimParameters(unsigned int &period, unsigned int &prescaler, const unsigned int frequency)
{
    period = CORE_CLOCK / frequency;
    prescaler = 1;
    while (period/prescaler >= MAX_PERIOD)
    {
        prescaler *= 2;
    }

    period = period / prescaler - 1;
    prescaler = prescaler - 1;
}

void setDutyCycle(TIM_TypeDef* tim, const unsigned int &dutyCycle)
{
    unsigned int period, prescaler;

    period = (tim->ARR) + 1;
    tim->CCR1 = dutyCycle * period / 100;
}
