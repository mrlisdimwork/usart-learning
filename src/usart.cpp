#include "stm32f4xx_conf.h"
#include "usart.h"

void USART_SendString (USART_TypeDef* usart, char* string)
{
    int i = 0;
    while (string[i])
    {
        USART_SendData(usart, string[i]);

        short isSended = 0;
        while (!isSended)
            isSended = USART_GetFlagStatus(usart, USART_SR_TC);

        i++;
    }
}
