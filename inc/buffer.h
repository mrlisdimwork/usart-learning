#ifndef BUFFER_H_INCLUDED
#define BUFFER_H_INCLUDED

#define OK 0
#define IS_SET strncmp(buffer.elem, "set", 3)
#define IS_GET strncmp(buffer.elem, "get", 3)
#define IS_HELP strncmp(buffer.elem, "help", 4)
#define IS_FREQ strncmp(buffer.elem + 4, "frequency", 9)
#define IS_DUTY strncmp(buffer.elem + 4, "duty cycle", 10)
#define BUFFER_SIZE 256
#define LINE_END 0x0A

struct Buffer
{
    char elem[BUFFER_SIZE];
    unsigned int nLastWritedElem = 0;
    unsigned int isReady = 0;
};

void addToBuffer(Buffer &buffer, uint8_t receivedByte);
void parseBuffer(Buffer &buffer, int captDutyCycle, int captFrequency);

#endif /* BUFFER_H_INCLUDED */
