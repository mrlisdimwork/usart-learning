#ifndef PWM_H_INCLUDED
#define PWM_H_INCLUDED

#define CORE_CLOCK (SystemCoreClock/4) * 2
#define REQUIRED_FREQUENCY 2000 //Hz > 2000
#define MAX_PERIOD 65536

void calcTimParameters(unsigned int &period, unsigned int &prescaler, const unsigned int frequency);
void setDutyCycle(TIM_TypeDef* tim, const unsigned int &dutyCycle);

#endif /* PWM_H_INCLUDED */
