#ifndef USART_H_INCLUDED
#define USART_H_INCLUDED

#define COUNT_OF_BYTES 7
#define USART UART4

void USART_SendString (USART_TypeDef* usart, char* string);

#endif /* USART_H_INCLUDED */
